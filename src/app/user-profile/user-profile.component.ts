import { Component, OnInit } from '@angular/core';
import {AuthService} from '../core/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  updateForm: FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              public auth: AuthService) { }

  ngOnInit() {
    this.initForm();
    this.ageCalculation();
  }

  initForm() {
    this.updateForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      city: ['', [Validators.required]],
      country: ['', [Validators.required]],
    })
  }

  onSubmit() {
    const firstName = this.updateForm.get('firstName').value;
    const lastName = this.updateForm.get('lastName').value;
    const city = this.updateForm.get('city').value;
    const country = this.updateForm.get('country').value;

    const additionalUserInfo = {
      firstName: firstName, lastName: lastName, city: city, country: country};

    this.auth.UpdateUserInfos(this.auth.userData, additionalUserInfo)
        .then(res => $.notify({
              title: '<strong>Informations successfully updated !</strong>',
              message: ''
            }, {
              type: 'success'
            })
        )
        .catch(err => $.notify({
              title: '<strong>Login failed</strong>',
              message: err.message
            }, {
              type: 'danger'
            })
        );
  }

  ageCalculation() {
      //
  }

}
