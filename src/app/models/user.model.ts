import {Product} from "./product.model";

export interface User {
    uid: string;
    email: string;
    role: string;
    firstName: string;
    lastName: string;
    birthDate: string;
    city: string;
    country: string;
    balance: number;
    isShopping: boolean;
    shoppingCart: Product[];
}
