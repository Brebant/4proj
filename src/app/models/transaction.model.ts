import {Product} from './product.model';

export class Transaction {
    id: string;
    listProducts: Product [];
    totalPrice: number;
    userId: number;
    date: string;
}
