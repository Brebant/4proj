export class Product {
    id: any;
    category: string;
    name: string;
    positionX: number;
    positionY: number;
    price: number;
    quantity: number;
    section: string;
    shelf: number;
    numberOfItemInCart: number;
}
