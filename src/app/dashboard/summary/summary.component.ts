import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../core/users.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {TransactionsService} from '../../core/transactions.service';
import {ProductsService} from '../../core/products.service';
import {User} from "../../models/user.model";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  numberOfClients: number;
  numberOfTransactions: number;
  numberOfProducts: number;
  outOfStocks: any;
  users: User[];

  constructor(public usersService: UsersService,
              public transactionsService: TransactionsService,
              public productsService: ProductsService) { }

  ngOnInit(): void {

    this.usersService.getNumberOfUsers().subscribe(data => {
      this.numberOfClients = data.length;
    }, error => console.error(error));

    this.transactionsService.getNumberOfTransactions().subscribe(data => {
      this.numberOfTransactions = data.length;
    }, error => console.error(error));

    this.productsService.getNumberOfProducts().subscribe(data => {
      this.numberOfProducts = data.length;
    }, error => console.error(error));
    
  this.productsService.getOutOfStocks().subscribe(data => {
      this.outOfStocks = data.length;
    }, error => console.error(error));

    this.usersService.getActiveUsers().subscribe(data => {
      this.users = data.map(e => {
        return {
          uid: e.payload.doc.id,
          ...(e.payload.doc.data() as object),
        } as User;
      });
    }, error => console.error(error));
  }
}
