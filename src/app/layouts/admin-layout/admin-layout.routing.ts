import { Routes } from '@angular/router';

import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { IconsComponent } from '../../icons/icons.component';
import {SummaryComponent} from '../../dashboard/summary/summary.component';
import {ClientsComponent} from '../../clients/clients.component';
import {TransactionsComponent} from '../../transactions/transactions.component';
import {ClientDetailsComponent} from '../../clients/client-details/client-details.component';
import {AngularFireAuthGuard, redirectUnauthorizedTo} from '@angular/fire/auth-guard';
import {ProductsComponent} from '../../products/products.component';
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: '',   redirectTo: '/summary', pathMatch: 'full' }, // redirect to `first-component`
    { path: 'summary',      component: SummaryComponent , canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }},
    { path: 'user-profile',   component: UserProfileComponent  , canActivate: [AngularFireAuthGuard]},
    { path: 'icons',          component: IconsComponent  , canActivate: [AngularFireAuthGuard]},
    { path: 'clients',        component: ClientsComponent  , canActivate: [AngularFireAuthGuard]},
    { path: 'transactions',   component: TransactionsComponent  , canActivate: [AngularFireAuthGuard]},
    { path: 'client-details',   component: ClientDetailsComponent  , canActivate: [AngularFireAuthGuard]},
    { path: 'products',   component: ProductsComponent  , canActivate: [AngularFireAuthGuard]},


];
