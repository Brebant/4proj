import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../core/auth.service';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private auth: AuthService,
              private ngZone: NgZone) { }

  ngOnInit(): void {
    if (this.auth.isLoggedIn) {
      this.router.navigate(['summary']).then();
    }
    this.initForm();
  }

  initForm() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      city: ['', [Validators.required]],
      country: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
    })
  }

  onSubmit() {
    const email = this.registerForm.get('email').value;
    const password = this.registerForm.get('password').value;
    const firstName = this.registerForm.get('firstName').value;
    const lastName = this.registerForm.get('lastName').value;
    const city = this.registerForm.get('city').value;
    const country = this.registerForm.get('country').value;
    const birthDate = this.registerForm.get('birthDate').value;

    const additionalUserInfo = {
      firstName: firstName, lastName: lastName, city: city, country: country, birthDate: birthDate, role: 'admin', isShopping: false, shoppingCart: []};
    this.auth.signUp(email, password, additionalUserInfo)
        .then()
        .catch(err => $.notify({
          title: '<strong>Login failed</strong>',
          message: err.message
        }, {
          type: 'danger'
        })
    );
  }

  alreadyRegistered() {
      this.ngZone.run(() => {
        this.router.navigate(['/login']).then();
      });
  }
}
