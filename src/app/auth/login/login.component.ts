import {Component, NgZone, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../core/auth.service';

declare var $: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    signinForm: FormGroup;

    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private auth: AuthService,
                private ngZone: NgZone) {
    }

    ngOnInit(): void {
        if (this.auth.isLoggedIn) {
            this.router.navigate(['summary']).then();
        }
        this.initForm();
    }

    initForm() {
        this.signinForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
        })
    }

    onSubmit() {
        const email = this.signinForm.get('email').value;
        const password = this.signinForm.get('password').value;
        this.auth.signIn(email, password)
            .then()
            .catch(err => {
                console.error(err);
                $.notify({
                    title: '<strong>Login failed</strong>',
                    message: err.message
                }, {
                    type: 'danger'
                })
            });
    }


    notRegister() {
        this.ngZone.run(() => {
            this.router.navigate(['/register']).then();
        });
    }

}
