import {Injectable, NgZone} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
      private afs: AngularFirestore,
      private router: Router,
      public ngZone: NgZone // NgZone service to remove outside scope warning
  ) {}

    getProducts() {
        return this.afs.collection('Products').snapshotChanges();
    }

    UpdateProductInfos(productId, productInfos) {
      const productsRef: AngularFirestoreDocument<any> = this.afs.doc(`Products/${productId}`);
      return productsRef.set(productInfos, {
        merge: true
      })
    }

    addProduct(product) {
    return this.afs.collection('Products').add(product).then()
    }

    getNumberOfProducts() {
      return this.afs.collection('Products').snapshotChanges()
    }

    getOutOfStocks() {
      return this.afs.collection('Products', ref => ref.where('quantity', '==', 0)).snapshotChanges()
    }

    getTotalSales() {
      return this.afs.collection('Transaction').snapshotChanges();
    }

    deleteProducts(products) {
    const docRef = this.afs.collection('Products');
    for (const product of products) {
     docRef.doc(product.id).delete().then(res => console.log(res), err => console.error(err))
    }
  }
}
