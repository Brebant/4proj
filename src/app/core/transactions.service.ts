import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(private afs: AngularFirestore) { }

  getTransactions() {
    return this.afs.collection('Transaction').snapshotChanges();
  }

  getNumberOfTransactions() {
    return this.afs.collection('Transaction').snapshotChanges()
  }
}
