import {Injectable, NgZone} from '@angular/core';
import {User} from '../models/user.model';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    userData: any; // Save logged in user data
    userInfos: any;

    constructor(
        private afAuth: AngularFireAuth,
        private afs: AngularFirestore,
        private router: Router,
        public ngZone: NgZone // NgZone service to remove outside scope warning
    ) {
        /* Saving user data in localstorage when logged in and setting up null when logged out */
        this.afAuth.authState.subscribe(user => {
            if (user) {
                this.userData = user;
                const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Profile/${user.uid}`);
                userRef.valueChanges()
                    .subscribe(
                        res => this.userInfos = res, error => console.error(error)
                    );
                localStorage.setItem('user', JSON.stringify(this.userData));
                JSON.parse(localStorage.getItem('user'));

            } else {
                localStorage.setItem('user', null);
                this.userData = null;
                JSON.parse(localStorage.getItem('user'));
            }
        })
    }

    // Returns true when user is logged in and email is verified
    get isLoggedIn(): boolean {
        const user = JSON.parse(localStorage.getItem('user'));
        return (user !== null);
    }

    // Sign up with email/password
    signUp(email, password, additionalUserInfo) {
        return this.afAuth.createUserWithEmailAndPassword(email, password)
            .then((result) => {
                this.router.navigate(['login']).then();
                this.SetUserData(result.user)
                    .then(() => {
                        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Profile/${result.user.uid}`);
                        userRef.set(additionalUserInfo, {
                            merge: true
                        }).then()
                    }).catch((error) => {
                    window.alert((error.message))
                });
            })
    }

    // Sign in with email/password
    signIn(email, password) {
        return this.afAuth.signInWithEmailAndPassword(email, password)
            .then((result) => {
                this.ngZone.run(() => {
                    this.router.navigate(['summary']).then();
                });
                this.SetUserData(result.user).then();
            })
    }

    // Reset Forggot password
    ForgotPassword(passwordResetEmail) {
        return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
            .then(() => {
                window.alert('Password reset email sent, check your inbox.');
            }).catch((error) => {
                window.alert(error)
            })
    }

    /* Setting up user data when sign in with username/password, sign up with username/password and sign in with social auth
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
    SetUserData(user) {
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Profile/${user.uid}`);
        const userData: { uid: any; email: any } = {
            uid: user.uid,
            email: user.email,
        };
        return userRef.set(userData, {
            merge: true
        })
    }

    // Sign out
    SignOut() {
        return this.afAuth.signOut().then(() => {
            localStorage.removeItem('user');
            this.userInfos = null;
            this.router.navigate(['login']).then();
        })
    }

    UpdateUserInfos(user, additionalUserInfo) {
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Profile/${user.uid}`);
        return userRef.set(additionalUserInfo, {
            merge: true
        }).then((res => {console.log(additionalUserInfo)}))
    }

}
