import {Injectable, NgZone} from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {


  constructor(private afs: AngularFirestore,
              private router: Router,
              public ngZone: NgZone // NgZone service to remove outside scope warning
  ) { }

  getUsers() {
    return this.afs.collection('Profile').snapshotChanges();
  }

  getActiveUsers() {
    return this.afs.collection('Profile',ref => ref.where('isShopping', '==', true)).snapshotChanges();
  }

  UpdateUserInfos(userUId, userInfos) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`Profile/${userUId}`);
    return userRef.set(userInfos, {
      merge: true
    }).then((res => {console.log(userInfos)}))
  }

  getNumberOfUsers() {
    return this.afs.collection('Profile', ref => ref.where('role', '==', 'customer')).snapshotChanges()
  }
}
