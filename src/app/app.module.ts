import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import {MatButtonModule} from '@angular/material/button';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import { ClientsComponent } from './clients/clients.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { ClientDetailsComponent } from './clients/client-details/client-details.component';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AuthGuard} from './core/auth.guard';
import {AuthService} from './core/auth.service';
import { ProductsComponent } from './products/products.component';
import {ButtonModule, DialogModule, DropdownModule, MultiSelectModule, SliderModule, TableModule, TreeTableModule} from 'primeng';
import {ProductsService} from './core/products.service';
// import {DropdownModule, MultiSelectModule, SliderModule, TableModule} from 'primeng';


@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        ComponentsModule,
        RouterModule,
        AppRoutingModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatRippleModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        AngularFireModule.initializeApp(environment.firebase),
        SliderModule,
        MultiSelectModule,
        TableModule,
        DropdownModule,
        ButtonModule,
        DialogModule,
        TreeTableModule,
    ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    RegisterComponent,
    ClientsComponent,
    TransactionsComponent,
    ClientDetailsComponent,
    ProductsComponent,
  ],
  providers: [AuthGuard, AuthService, ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }

