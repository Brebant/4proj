import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng';
import {Product} from '../models/product.model';
import {ProductsService} from '../core/products.service';
import {Transaction} from '../models/transaction.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
declare var $: any;


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[];
  transactions: Transaction[];
  cols: any[];
  categories: SelectItem[];
  sections: SelectItem[];
  product: Product = {} as any;
  selectedProducts: Product [];
  displayDialog: boolean;
  addProductForm: FormGroup;

  constructor(public productsService: ProductsService,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.productsService.getProducts().subscribe(data => {
      this.products = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...(e.payload.doc.data() as object),
        } as Product;
      });
      this.initDropdowns();
    }, error => console.error(error));

    this.cols = [
      { field: 'id', header: 'Id' },
      { field: 'name', header: 'Name' },
      { field: 'section', header: 'Section' },
      { field: 'category', header: 'Category' },
      { field: 'price', header: 'Price' },
      { field: 'quantity', header: 'Quantity' },
      { field: 'shelf', header: 'Shelf' },
      { field: 'positionX', header: 'PositionX' },
      { field: 'positionY', header: 'PositionY' },
    ];

    this.initFormProduct();
  }

  initDropdowns() {
    this.categories = [];
    this.sections = [];
    for (const product of this.products) {
      if (this.categories.find((test) => test.label === product.category) === undefined) {
        this.categories.push({ label: product.category, value: product.category });
      }
      if (this.sections.find((test) => test.label === product.section) === undefined) {
        this.sections.push({ label: product.section, value: product.section });
      }
    }
  }

  initFormProduct() {
    this.addProductForm = this.formBuilder.group({
      'name': new FormControl('', Validators.required),
      'section': new FormControl('', Validators.required),
      'category': new FormControl('', Validators.required),
      'price': new FormControl('', Validators.required),
      'quantity': new FormControl('', Validators.required),
      'shelf': new FormControl('', Validators.required),
      'positionX': new FormControl('', Validators.required),
      'positionY': new FormControl('', Validators.required),
    });
  }

  addProduct() {
    this.productsService.addProduct(this.product)
        .then(res => {
          console.log(res);
          this.product = {} as any;
          $.notify({
            title: '<strong>Product successfully added !</strong>',
            message: ''
          }, {
            type: 'success'
          })
        }, err => {
          console.error(err);
          $.notify({
            title: '<strong>An error occurred, please retry !</strong>',
            message: ''
          }, {
            type: 'error'
          })
        });
  }

  onRowEditSave(productId, productInfos) {
    this.productsService.UpdateProductInfos(productId, productInfos)
        .then(res => {
          $.notify({
            title: '<strong>Product successfully edited !</strong>',
            message: ''
          }, {
            type: 'success'
          })
        }, err => {
          console.error(err);
          $.notify({
            title: '<strong>An error occurred, please retry !</strong>',
            message: ''
          }, {
            type: 'error'
          })
        });
  }

  deleteProducts() {
    this.productsService.deleteProducts(this.selectedProducts);
    this.selectedProducts = [];
  }

  showDialogAddProduct() {
    this.displayDialog = true;
  }

  closeDialogAddProduct() {
    this.displayDialog = false;
    this.product = {} as any;
  }

/**
  getTotalSales() {
    let value = 0
    for (const transaction of this.transactions) {
      for (const product of transaction.listProducts) {
        for (const categorie of this.categories) {
            value += product.numberOfItemInCart;
            console.log(value);
        }
      }
    }
  }
*/
}

