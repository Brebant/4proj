import { Component, OnInit } from '@angular/core';
import {Product} from '../models/product.model';
import {SelectItem} from 'primeng';
import {User} from '../models/user.model';
import {UsersService} from '../core/users.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  users: User[];

  cols: any[];

  user: User = {} as any;

  roles: SelectItem[];

  constructor(private usersService: UsersService) { }

  ngOnInit(): void {
    this.usersService.getUsers().subscribe(data => {
      this.users = data.map(e => {
        return {
          uid: e.payload.doc.id,
          ...(e.payload.doc.data() as object),
        } as User;
      });
    }, error => console.error(error));

    this.cols = [
      { field: 'id', header: 'Id' },
      { field: 'email', header: 'Email' },
      { field: 'role', header: 'Role' },
      { field: 'firstName', header: 'FirstName' },
      { field: 'lastName', header: 'LastName' },
      { field: 'birthDate', header: 'BirthDate' },
      { field: 'city', header: 'City' },
      { field: 'country', header: 'Country' },
      { field: 'balance', header: 'Balance' },
    ];

    this.roles = [
      { label: 'Admin', value: 'admin' },
      { label: 'Customer', value: 'customer' },
    ]
  }

  onRowEditSave(userUId, userInfos) {
    this.usersService.UpdateUserInfos(userUId, userInfos).catch(err => console.error(err));
  }

}
