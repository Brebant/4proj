import { Component, OnInit } from '@angular/core';
import {Transaction} from '../models/transaction.model';
import {TransactionsService} from '../core/transactions.service';
import {Tree, TreeNode} from 'primeng';
import {Product} from '../models/product.model';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css'],
})
export class TransactionsComponent implements OnInit {

  transactions: Transaction[];

  cols: any[];

  colsProducts: any[];

  constructor(public transactionsService: TransactionsService) { }

  ngOnInit(): void {
    this.transactionsService.getTransactions().subscribe(data => {
      this.transactions = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...(e.payload.doc.data() as object),
        } as Transaction;
      });
    }, error => console.error(error));

    this.cols = [
      { field: 'id', header: 'Id' },
      { field: 'userId', header: 'User Id' },
      { field: 'date', header: 'Date' },
      { field: 'listProducts', header: 'Products' },
      { field: 'totalPrice', header: 'TotalPrice' },
    ];

    this.colsProducts = [
      { field: 'id', header: 'Id' },
      { field: 'name', header: 'Name' },
      { field: 'section', header: 'Section' },
      { field: 'category', header: 'Category' },
      { field: 'price', header: 'Price' },
      { field: 'quantity', header: 'Quantity' },
      { field: 'shelf', header: 'Shelf' },
      { field: 'positionX', header: 'PositionX' },
      { field: 'positionY', header: 'PositionY' },
    ];
  }
}
