// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase : {
    /**
    apiKey: 'AIzaSyCPXCLZOoxX15wsLL8K30G_ghYZYHG7KTg',
    authDomain: 'proj-ac7e3.firebaseapp.com',
    databaseURL: 'https://proj-ac7e3.firebaseio.com',
    projectId: 'proj-ac7e3',
    storageBucket: 'proj-ac7e3.appspot.com',
    messagingSenderId: '305641151790',
    appId: '1:305641151790:web:0da62315ce8debe606aebf',
    measurementId: 'G-2R5BZ91SYX'
     **/
    apiKey: 'AIzaSyCvTa_VuKyho71eGX5cPw4fCiaaIh17VQI',
    authDomain: 'supermarket-c24f6.firebaseapp.com',
    databaseURL: 'https://supermarket-c24f6.firebaseio.com',
    projectId: 'supermarket-c24f6',
    storageBucket: 'supermarket-c24f6.appspot.com',
    messagingSenderId: '817289837551',
    appId: '1:817289837551:web:dd8658d9e1761c0fed941a',
    measurementId: 'G-BSRTX1PMS9'
}
};
